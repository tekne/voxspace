/// A block
#[derive(Debug, Copy, Clone, Eq, PartialEq, Hash, Ord, PartialOrd)]
#[repr(transparent)]
pub struct Block(u64);

impl Block {
    /// The empty block ID
    pub const EMPTY: Block = Block(0);
    /// Get whether this block is null
    pub const fn is_null(self) -> bool {
        self.0 == 0
    }
    /// Create a new block
    pub const fn new(id: BlockId, meta: MetaId, flags: BlockFlags) -> Block {
        Block(id.0 << 22 | (meta.0 as u64) << 6 | flags.0 as u64)
    }
    /// Get whether this block is a standard, visible cube
    ///
    /// If this is false, the block is either not rendered or has a custom rendering group/implementation
    pub const fn is_cube(self) -> bool {
        self.0 & 1 == 1
    }
    /// Get whether this block is opaque
    pub const fn is_opaque(self) -> bool {
        self.0 & 2 == 1
    }
    /// Get whether this block is solid
    ///
    /// If this is false, the block either has no physics or has a custom physics group/implementation
    pub const fn is_solid(self) -> bool {
        self.0 & 3 == 1
    }
    /// Get this block's flags
    pub const fn flags(self) -> BlockFlags {
        BlockFlags::new_trunc(self.0)
    }
    /// Get this block's ID
    pub const fn id(self) -> BlockId {
        BlockId::new_trunc(self.0 >> 22)
    }
    /// Get this block's metadata ID
    pub const fn meta(self) -> MetaId {
        MetaId::new_trunc(self.0 >> 6)
    }
}

/// Block flags (6 bits)
#[derive(Debug, Copy, Clone, Eq, PartialEq, Hash, Ord, PartialOrd)]
pub struct BlockFlags(u8);

impl BlockFlags {
    /// Get the bits in a BlockFlags
    pub const BITS: u32 = 6;
    /// Get the mask for a BlockFlags
    pub const MASK: u64 = (1u64 << Self::BITS) - 1;
    /// Get whether this flag set is null
    pub const fn is_null(self) -> bool {
        self.0 == 0
    }
    /// Get whether this block is a standard, visible cube
    ///
    /// If this is false, the block is either not rendered or has a custom rendering group/implementation
    pub const fn is_cube(self) -> bool {
        self.0 & 1 == 1
    }
    /// Get whether this block is opaque
    pub const fn is_opaque(self) -> bool {
        self.0 & 2 == 1
    }
    /// Get whether this block is solid
    ///
    /// If this is false, the block either has no physics or has a custom physics group/implementation
    pub const fn is_solid(self) -> bool {
        self.0 & 3 == 1
    }
    /// Truncate a u64 to a BlockFlags
    pub const fn new_trunc(bits: u64) -> BlockFlags {
        BlockFlags((bits & Self::MASK) as u8)
    }
}

/// A block ID (42 bits)
#[derive(Debug, Copy, Clone, Eq, PartialEq, Hash, Ord, PartialOrd)]
#[repr(transparent)]
pub struct BlockId(u64);

impl BlockId {
    /// Get the bits in a BlockId
    pub const BITS: u32 = 42;
    /// Get the mask for a BlockId
    pub const MASK: u64 = (1u64 << Self::BITS) - 1;
    /// Get whether this BlockId is null
    pub const fn is_null(self) -> bool {
        self.0 == 0
    }
    /// Truncate a u64 to a BlockId
    pub const fn new_trunc(bits: u64) -> BlockId {
        BlockId(bits & Self::MASK)
    }
}

/// A metadata ID (16 bits)
#[derive(Debug, Copy, Clone, Eq, PartialEq, Hash, Ord, PartialOrd)]
#[repr(transparent)]
pub struct MetaId(u16);

impl MetaId {
    /// Get the bits in a MetaId
    pub const BITS: u32 = 16;
    /// Get the mask for a MetaId
    pub const MASK: u64 = (1u64 << Self::BITS) - 1;
    /// Get whether this MetaId is null
    pub const fn is_null(self) -> bool {
        self.0 == 0
    }
    /// Truncate a u64 to a MetaId
    pub const fn new_trunc(bits: u64) -> MetaId {
        MetaId(bits as u16)
    }
}

#[cfg(test)]
mod test {
    use super::*;
    use rand::{Rng, SeedableRng};
    use rand_xoshiro::Xoroshiro128Plus;
    #[test]
    fn block_construction() {
        let mut rng = Xoroshiro128Plus::seed_from_u64(0x12345);
        for flags in 0..(1 << 6) {
            let flags = BlockFlags::new_trunc(flags);
            for _ in 0..(1 << 6) {
                let id = BlockId::new_trunc(rng.gen());
                let meta = MetaId::new_trunc(rng.gen());
                let block = Block::new(id, meta, flags);
                assert_eq!(block.id(), id);
                assert_eq!(block.meta(), meta);
                assert_eq!(block.flags(), flags)
            }
        }
    }
}
