use bevy::{
    prelude::*,
    render::{mesh::Indices, pass::ClearColor, pipeline::PrimitiveTopology},
};
use bevy_rapier3d::physics::RapierPhysicsPlugin;
use building_blocks::{core::prelude::*, mesh::*, storage::prelude::*};
use rapier3d::{
    dynamics::{IntegrationParameters, RigidBodyBuilder},
    geometry::ColliderBuilder,
    pipeline::PhysicsPipeline,
};

fn main() {
    App::build()
        .add_resource(ClearColor(Color::rgb(0.0, 0.0, 0.0)))
        .add_resource(Msaa::default())
        .add_plugins(DefaultPlugins)
        .add_plugin(RapierPhysicsPlugin)
        .add_plugin(bevy_flycam::PlayerPlugin)
        .add_startup_system(setup_graphics.system())
        .add_startup_system(setup_physics.system())
        .add_startup_system(enable_physics_profiling.system())
        .add_resource(BallState::default())
        .add_system(ball_spawner.system())
        .run();
}

fn enable_physics_profiling(mut pipeline: ResMut<PhysicsPipeline>) {
    pipeline.counters.enable()
}

fn setup_graphics(commands: &mut Commands) {
    commands.spawn(LightBundle {
        transform: Transform::from_translation(Vec3::new(1000.0, 100.0, 2000.0)),
        ..Default::default()
    });
}

pub fn setup_physics(
    commands: &mut Commands,
    asset_server: Res<AssetServer>,
    mut materials: ResMut<Assets<StandardMaterial>>,
    mut meshes: ResMut<Assets<Mesh>>,
) {
    use bevy_rapier3d::na::Point3;

    let rigid_body = RigidBodyBuilder::new_static().translation(0.0, 0.0, 0.0);

    let PosNormTexMesh {
        positions,
        normals,
        tex_coords,
        indices,
    } = make_pyramid();

    let collider_vertices = positions
        .iter()
        .map(|&[x, y, z]| Point3::new(x, y, z))
        .collect();
    let collider_indices = indices.chunks(3).map(|c| [c[0], c[1], c[2]]).collect();

    let collider = ColliderBuilder::trimesh(collider_vertices, collider_indices);

    let mut pyramid_mesh = Mesh::new(PrimitiveTopology::TriangleList);
    pyramid_mesh.set_attribute(Mesh::ATTRIBUTE_POSITION, positions);
    pyramid_mesh.set_attribute(Mesh::ATTRIBUTE_NORMAL, normals);
    pyramid_mesh.set_attribute(Mesh::ATTRIBUTE_UV_0, tex_coords);
    pyramid_mesh.set_indices(Some(Indices::U32(indices)));

    let uv_checker_handle: Handle<Texture> = asset_server.load("uv_checker.png");
    let material_handle = materials.add(StandardMaterial {
        albedo: Color::rgba(1.0, 0.3, 0.3, 0.5),
        albedo_texture: Some(uv_checker_handle.clone()),
        ..Default::default()
    });
    let pbr = PbrBundle {
        mesh: meshes.add(pyramid_mesh),
        material: material_handle.clone(),
        visible: Visible {
            is_transparent: true,
            ..Default::default()
        },
        ..Default::default()
    };

    commands
        .spawn((
            rigid_body,
            Transform::identity(),
            GlobalTransform::identity(),
        ))
        .with_children(|parent| {
            parent.spawn(pbr);
            parent.spawn((collider,));
        });
}

struct BallState {
    seconds_until_next_spawn: f32,
    seconds_between_spawns: f32,
    balls_spawned: usize,
    max_balls: usize,
}

impl Default for BallState {
    fn default() -> Self {
        Self {
            seconds_until_next_spawn: 0.5,
            seconds_between_spawns: 2.0,
            balls_spawned: 0,
            max_balls: 10,
        }
    }
}

fn ball_spawner(
    commands: &mut Commands,
    integration_parameters: Res<IntegrationParameters>,
    mut ball_state: ResMut<BallState>,
    mut meshes: ResMut<Assets<Mesh>>,
    mut materials: ResMut<Assets<StandardMaterial>>,
) {
    if ball_state.balls_spawned >= ball_state.max_balls {
        return;
    }

    // NOTE: The timing here only works properly with `time_dependent_number_of_timesteps`
    // disabled, as it is for examples.
    ball_state.seconds_until_next_spawn -= integration_parameters.dt;
    if ball_state.seconds_until_next_spawn > 0.0 {
        return;
    }
    ball_state.seconds_until_next_spawn = ball_state.seconds_between_spawns;

    let rigid_body = RigidBodyBuilder::new_dynamic()
        .translation(3.0, 30.0, 0.0)
        .linvel(0.1, 0.1, 0.1);
    let collider = ColliderBuilder::ball(3.0).restitution(0.5);

    let ball_factor = ball_state.balls_spawned as f32 / ball_state.max_balls as f32;

    commands
        .spawn((
            rigid_body,
            Transform::identity(),
            GlobalTransform::identity(),
        ))
        .with_children(|parent| {
            parent.spawn((collider,));
            parent.spawn(PbrBundle {
                mesh: meshes.add(Mesh::from(shape::Icosphere {
                    radius: 3.0,
                    subdivisions: 4,
                })),
                material: materials.add(Color::rgb(ball_factor, 0.3, 0.8).into()),
                transform: Transform::from_translation(Vec3::new(0.0, 0.0, 0.0)),
                ..Default::default()
            });
        });

    ball_state.balls_spawned += 1;
}

#[derive(Default, Clone, Copy)]
struct Voxel(u8);

impl MergeVoxel for Voxel {
    type VoxelValue = u8;
    fn voxel_merge_value(&self) -> Self::VoxelValue {
        self.0
    }
}

impl IsOpaque for Voxel {
    fn is_opaque(&self) -> bool {
        true
    }
}

impl IsEmpty for Voxel {
    fn is_empty(&self) -> bool {
        self.0 == 0
    }
}

fn make_pyramid() -> PosNormTexMesh {
    let extent = Extent3i::from_min_and_shape(PointN([-20; 3]), PointN([40; 3])).padded(1);
    let mut voxels = Array3::fill(extent, Voxel::default());
    for i in 0..40 {
        let level = Extent3i::from_min_and_shape(PointN([i - 20; 3]), PointN([40 - i, 1, 40 - i]));
        voxels.fill_extent(&level, Voxel((i % 4) as u8 + 1));
    }

    let mut greedy_buffer = GreedyQuadsBuffer::new(extent);
    greedy_quads(&voxels, &extent, &mut greedy_buffer);

    let mut mesh_buf = PosNormTexMesh::default();
    for group in greedy_buffer.quad_groups.iter() {
        for quad in group.quads.iter() {
            group
                .face
                .add_quad_to_pos_norm_tex_mesh(quad, &mut mesh_buf)
        }
    }

    mesh_buf
}
